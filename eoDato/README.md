# Tasker eoDato

A task to convert the day and month into Esperanto and put them into variables that can be used in a MinimalisticText widget

## Necessary plugins
* [Minimalistic Text: widgets](https://play.google.com/store/apps/details?id=de.devmil.minimaltext)