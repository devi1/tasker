# Tasker
A collection of my personal Tasker projects.

## List of Tasks

#### Rhapsody
A set of tasks to interact with the Rhapsody API

#### Pastee
A task to create a new paste on pastee.org

#### eoDato
A task to convert the current day of the week and current month into Esperanto, and insert them into variables that can be used in a MinimalisticText widget