# Pastee

A Tasker task to create a new paste on pastee.org and return the url of the newly created paste

## Necessary plugins
* [RESTask](https://play.google.com/store/apps/details?id=com.freehaha.restask)

## Descriptions of individual tasks

### pstNewPastee
Creates a new paste on pastee.org

### pstExamplePaste
Example task which creates a new paste and opens it in the default browser.

## Usage

pstNewPaste should be called from a separate task using the Perform Task action.

**%par1** is required, and should be set to the desired content of the new paste.

**%par2** is optional, and if set, should contain the amount of time in days before the paste expires. By default, pastes will expire after 1 day.