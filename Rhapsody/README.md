# Tasker Rhapsody API

A set of Tasker tasks to interact with the Rhapsody API

## Necessary plugins
* [RESTask](https://play.google.com/store/apps/details?id=com.freehaha.restask)

## Descriptions of individual tasks

### rhapAuth
Authorizes account with Rhapsody and obtains access token and refresh token.

### rhapAuthRefresh
Refreshes previously obtained access and refresh tokens. This is called once the access token has expired.

### rhapSetVars
This is basically the configuration which holds your API key information, and login information. This is called at the beginning of each task in order to ensure the information stays current across all tasks.

### rhapTemplateAuth
This is a template task, which contains the basic procedures used when an API call requires authentication.

### rhapTemplateNoAuth
This is a template task, which contains the basic procedures used when an API call does not require authentication.